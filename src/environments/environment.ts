// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCHFpHRCs715cQh_7sQr0z4Xcrj6Ty3TCw',
    authDomain: 'billetera-qr-hb.firebaseapp.com',
    projectId: 'billetera-qr-hb',
    storageBucket: 'billetera-qr-hb.appspot.com',
    messagingSenderId: '385102520057',
    appId: '1:385102520057:web:6a3cfebad2ddf002190c28',
    measurementId: 'G-EWYMMZCHXP'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
