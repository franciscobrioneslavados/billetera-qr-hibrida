import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateQrInfoPageRoutingModule } from './update-qr-info-routing.module';

import { UpdateQrInfoPage } from './update-qr-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateQrInfoPageRoutingModule
  ],
  declarations: [UpdateQrInfoPage]
})
export class UpdateQrInfoPageModule {}
