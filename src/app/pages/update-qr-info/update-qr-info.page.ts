import {Component, Input, OnInit} from '@angular/core';
import {DataInterface} from '../../interfaces/data/data.interface';
import {FirestoreService} from '../../services/firestore/firestore.service';
import {ToasterService} from '../../services/toaster/toaster.service';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-update-qr-info',
  templateUrl: './update-qr-info.page.html',
  styleUrls: ['./update-qr-info.page.scss'],
})
export class UpdateQrInfoPage implements OnInit {
  @Input() doc: DataInterface;

  constructor(
    private firestoreService: FirestoreService,
    private toaster: ToasterService,
    private modalController: ModalController,
  ) {
  }

  ngOnInit() {
  }

  async onSaveInfo() {
    await this.firestoreService.update(this.doc.uid, this.doc);
    await this.toaster.successToast('Valores actualizados');
    await this.dismissModal();
  }

  async dismissModal(role?) {
    return this.modalController.dismiss({role});
  }
}
