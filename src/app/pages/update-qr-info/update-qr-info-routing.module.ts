import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateQrInfoPage } from './update-qr-info.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateQrInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateQrInfoPageRoutingModule {}
