import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {DataInterface, DocsExtraModel} from '../../interfaces/data/data.interface';
import {ToasterService} from '../../services/toaster/toaster.service';
import {FirestoreService} from '../../services/firestore/firestore.service';
import {Observable, ReplaySubject} from 'rxjs';
import {HomePage} from '../home/home.page';
import {UtilsService} from '../../services/utils/utils.service';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnDestroy, OnChanges {

  @Input() idDoc: string;
  createOrUpdate = false;
  docsExtra: DocsExtraModel[] = [];
  unSubscription$: Observable<any>;
  homePageComponent: HomePage;
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  // eslint-disable-next-line @typescript-eslint/member-ordering
  doc: DataInterface = new DataInterface();

  constructor(
    private modalController: ModalController,
    private toasterService: ToasterService,
    private firestoreService: FirestoreService,
    private utilService: UtilsService,
  ) {

  }

  ionViewDidEnter() {
    this.firestoreService.getData(this.idDoc).pipe(takeUntil(this.destroyed$)).subscribe(data => {
      this.doc = data as DataInterface;
      this.docsExtra = this.doc.extra.docs;
    });
  }

  async dismissModal(role?) {
    return this.modalController.dismiss({role});
  }

  changeActionCreateButton() {
    this.createOrUpdate = !this.createOrUpdate;
  }

  onRemoveDoc(position: number) {
    this.toasterService.presentToastWithOptions('¿ Seguro de querer continuar ?', 'middle').then((resolve) => {
      if (resolve === 'ok') {
        this.docsExtra.splice(position, 1);
      }
    });
  }

  onSaveDoc() {
    this.toasterService.presentToastWithOptions('¿ Seguro de querer guardar ?', 'middle').then(async (resolve) => {
      if (resolve === 'ok') {
        await this.firestoreService.update(this.doc.uid, this.doc);
        await this.toasterService.successToast('Datos guardados!');
        await this.dismissModal({role: 'ok'});
      } else {
        this.changeActionCreateButton();
      }
    });
  }

  async openFile() {
    const url = await this.utilService.openFile();
    this.openFileAndOpenPromptSavingData(url);
  }

  openFileAndOpenPromptSavingData(url) {
    const name = url.substring(url.lastIndexOf('/') + 1);
    const type = name.substring(name.lastIndexOf('.') + 1);
    this.docsExtra.push({
      path: url,
      name,
      type
    });
    this.toasterService.successToast('Documento añadido!').then();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.doc){
      changes.doc = changes.doc.currentValue;
    }
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }

  onItemSelected(event: MouseEvent) {
    console.log(event.target);
  }

}
