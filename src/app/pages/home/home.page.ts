import {Component, OnDestroy} from '@angular/core';
import {
  BarcodeScanner,
  BarcodeScanResult,
} from '@ionic-native/barcode-scanner/ngx';
import {
  ActionSheetController,
  AlertController,
  ModalController,
  PopoverController,
} from '@ionic/angular';
import {ToasterService} from 'src/app/services/toaster/toaster.service';
import {
  NgxQrcodeElementTypes,
  NgxQrcodeErrorCorrectionLevels,
} from '@techiediaries/ngx-qrcode';
import {DataInterface, ExtraDataModel} from '../../interfaces/data/data.interface';
import {FirestoreService} from '../../services/firestore/firestore.service';
import {InfoPage} from '../info/info.page';
import {UtilsService} from '../../services/utils/utils.service';
import {Browser} from '@capacitor/browser';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import {UpdateQrInfoPage} from '../update-qr-info/update-qr-info.page';
import {takeUntil} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';
import {LoaderService} from '../../services/loader/loader.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnDestroy {
  barcodeScanResult: BarcodeScanResult;
  qrEncode: any;
  scanDocuments: DataInterface[] = [];
  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  extraData: ExtraDataModel = new ExtraDataModel();
  isExtra: number;
  slideOpts = {
    initialSlide: 0,
    speed: 300,
    effect: 'coverflow',
    allowSlideNext: true,
    allowSlidePrev: true,
  };
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);

  constructor(
    private barcodeScanner: BarcodeScanner,
    private alertController: AlertController,
    private toastService: ToasterService,
    private popoverController: PopoverController,
    private actionSheetController: ActionSheetController,
    private firestoreService: FirestoreService,
    private modalController: ModalController,
    private utilService: UtilsService,
    private photoViewer: PhotoViewer,
    private loaderService: LoaderService
  ) {

  }

  ionViewDidEnter() {
    this.loaderService.presentLoading('Cargando información').then();
    this.loadData();
  }

  scanQR() {
    this.barcodeScanResult = null;
    this.barcodeScanner
      .scan()
      .then((barcodeScanResult) => {
        this.barcodeScanResult = barcodeScanResult as BarcodeScanResult;
        this.presentAlertPrompt().then();
      })
      .catch((err) => {
        console.error('Error', err);
      });
  }

  loadData() {
    this.firestoreService.getAllData().pipe(takeUntil(this.destroyed$)).subscribe((document) => {
      if (document) {
        this.scanDocuments = document.map((t) => ({
            id: t.payload.doc.id,
            ...t.payload.doc.data() as DataInterface,
          }));
      }
      this.loaderService.dismissLoading().then();
    }, async error => {
      this.loaderService.dismissLoading().then();
    });
  }

  async presentAlertPrompt(extra?: ExtraDataModel) {
    const alert = await this.alertController.create({
      cssClass: '',
      header: 'Menu de Opciones',
      inputs: [
        {
          name: 'name',
          type: 'text',
          placeholder: 'Pase de movilidad propio',
          label: 'Nombre',
        },
        {
          name: 'detail',
          type: 'textarea',
          placeholder: 'Opcional: Pase de movilidad 01-01-1988',
          label: 'Detalle',
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Guardar',
          handler: (data) => {
            const saveData = {
              name: data.name,
              detail: data.detail,
              url: this.barcodeScanResult ? this.barcodeScanResult.text : null,
              created: Date.now(),
              extra: extra ? extra : null,
            };
            this.saveOnDatabase(saveData).then();
          },
        },
      ],
    });
    await alert.present();
  }

  async saveOnDatabase(data) {
    await this.firestoreService.createData(data);
    await this.toastService.successToast('Información guardada!');
    this.loadData();
  }

  async presentActionSheet(doc: DataInterface, url?: any, extra?: any) {
    const buttonsUrl = [
      {
        text: 'Editar Valores',
        icon: 'create',
        handler: () => {
          this.updateQrInfo(doc);
        }
      },
      {
        text: 'Abrir enlace',
        icon: 'link',
        handler: () => {
          this.openLink(doc);
        }
      },
      {
        text: 'Mostrar QR',
        icon: 'qr-code',
        handler: () => {
        }
      },
      {
        text: 'Borrar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.onRemoveDoc(doc);
        }
      },
      {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
      }];
    const buttonsExtra = [
      {
        text: 'Editar / ver Archivos',
        icon: 'document',
        handler: async () => {
          await this.openOrEditDocuments(doc);
        }
      },
      {
        text: 'Borrar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.onRemoveDoc(doc);
        }
      },
      {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
      }];
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones ',
      cssClass: '',
      buttons: url ? buttonsUrl : buttonsExtra,
    });
    await actionSheet.present();
    // const {role} = await actionSheet.onDidDismiss();
  }

  async openLink(doc) {
    await Browser.open({url: doc.url});
  }

  openQR(event: PointerEvent) {
    const image = new Image();
    // eslint-disable-next-line @typescript-eslint/dot-notation
    image.src = event.srcElement['currentSrc'];
    this.photoViewer.show(image.src, 'QR Info', {closeButton: true, share: true});
  }

  async onRemoveDoc(doc) {
    await this.firestoreService.delete(doc.id);
    await this.toastService.successToast('Dato Borrado');
  }

  async openFile() {
    const url = await this.utilService.openFile();
    this.openFileAndOpenPromptSavingData(url);
  }

  openFileAndOpenPromptSavingData(url) {
    const extraData: ExtraDataModel = {docs: []};
    const name = url.substring(url.lastIndexOf('/') + 1);
    const type = name.substring(name.lastIndexOf('.') + 1);
    extraData.docs.push({
      path: url,
      name,
      type
    });
    this.presentAlertPrompt(extraData).then();
  }

  async openOrEditDocuments(doc: DataInterface) {
    const modal = await this.modalController.create({
      component: InfoPage,
      cssClass: '',
      componentProps: {
        idDoc: doc.uid,
      }
    });
    return await modal.present();
  }

  async updateQrInfo(doc: DataInterface) {
    const modal = await this.modalController.create({
      component: UpdateQrInfoPage,
      cssClass: '',
      componentProps: {
        doc,
      }
    });
    return await modal.present();
  }

  ngOnDestroy(): void {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
