import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {SQLite} from '@ionic-native/sqlite/ngx';
import {QrEncodePipe} from './pipes/qrencode/qrEncode.pipe';
import {FileChooser} from '@ionic-native/file-chooser/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {DocumentPicker} from '@ionic-native/document-picker/ngx';
import {Device} from '@ionic-native/device/ngx';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire/compat';
import {AngularFireDatabaseModule} from '@angular/fire/compat/database';
import {PhotoViewer} from '@ionic-native/photo-viewer/ngx';
import {AngularFireAuthModule} from '@angular/fire/compat/auth';

@NgModule({
  declarations: [AppComponent, QrEncodePipe],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],

  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    BarcodeScanner,
    SQLite,
    FileChooser,
    FilePath,
    DocumentPicker,
    Device,
    PhotoViewer,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
