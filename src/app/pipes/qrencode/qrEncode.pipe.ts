import { Pipe, PipeTransform } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Pipe({
  name: 'qrencode'
})
export class QrEncodePipe implements PipeTransform {

  constructor(
    private barcodeScanner: BarcodeScanner,
  ){}
  transform(value: unknown, ...args: unknown[]): unknown {
    return this.barcodeScanner.encode(this.barcodeScanner.Encode.TEXT_TYPE, value);
  }

}
