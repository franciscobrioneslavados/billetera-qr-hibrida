import {Injectable} from '@angular/core';
import {LoadingController} from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loading: any;
  constructor(
    public loadingController: LoadingController
  ) {
  }

  async presentLoading(message, duration?) {
    this.loading = await this.loadingController.create({
      cssClass: '',
      message,
      duration
    });
    this.loading.present();

    const {role, data} = await this.loading.onDidDismiss();
  }

  async dismissLoading() {
    await this.loading.dismiss();
  }
}
