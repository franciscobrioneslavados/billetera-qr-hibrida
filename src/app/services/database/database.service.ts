/* eslint-disable max-len */
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import {DataInterface} from '../../interfaces/data/data.interface';

@Injectable({
  providedIn: 'root',
})
export class DatabaseService {
  private dbInstance: SQLiteObject;
  private dbName = 'data.db';
  private dbTable = 'scans';
  private scanDocuments: DataInterface[];

  constructor(private platform: Platform, private sqlite: SQLite) {
    this.databaseConn();
  }

  databaseConn() {
    this.platform.ready().then(() => {
      this.sqlite
        .create({
          name: this.dbName,
          location: 'default',
        })
        .then((sqLite: SQLiteObject) => {
          this.dbInstance = sqLite;
          sqLite
            .executeSql(`CREATE TABLE IF NOT EXISTS ${this.dbTable} (id INTEGER PRIMARY KEY, name varchar(255), detail varchar(255), url text, created text, extra text)`, [])
            .then()
            .catch((error) => console.error(JSON.stringify(error)));
        })
        .catch((error) => console.error(JSON.stringify(error)));
    });
  }

  public addItem(values): Promise<any> {
    const created = Date.now();
    return this.dbInstance.executeSql(
      `INSERT INTO ${this.dbTable} (name, detail, url, created, extra) VALUES ('${values.name}', '${values.detail}', '${values.url}', '${created}', ${values.extra})`,
      []
    ).catch(e => {
      //this.dbInstance.executeSql(`DROP TABLE ${this.dbTable}`).then();
      console.error(JSON.stringify(e));
    });
  }

  getAllScans() {
    this.scanDocuments = [];
    return this.dbInstance.executeSql(`SELECT * FROM ${this.dbTable}`, []).then(res => {
      for (let i = 0; i < res.rows.length; i++) {
        this.scanDocuments.push(res.rows.item(i));
      }
      return this.scanDocuments;
    });
  }

  deleteScan(id: any) {
    this.dbInstance.executeSql(`
    DELETE FROM ${this.dbTable} WHERE _id = ${id}`, [])
      .then(() => {
        console.log('row delete', id);
      })
      .catch(e => {
        console.error(JSON.stringify(e));
      });
  }
}
