import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/compat/firestore';
import {DataInterface} from '../../interfaces/data/data.interface';
import {Device} from '@ionic-native/device/ngx';
import {SchemasObject} from '../../interfaces/schemas';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  private readonly collection = this.device.isVirtual === false ? this.device.uuid : '2f5e428d099a147a';

  constructor(
    private ngFirestore: AngularFirestore,
    private device: Device,
  ) {
    console.log('collection', this.collection);
  }

  async createData(data: DataInterface) {
    this.ngFirestore.collection(this.collection).add({}).then(resolve => {
      this.ngFirestore.collection(this.collection).doc(resolve.id).update({uid: resolve.id, ...data}).then().catch(error => {
        console.error(error);
      });
    });
    // return this.ngFirestore.collection(this.collection).doc(newDocRef.id).set({newDocRef.id, ...data});
  }

  getAllData() {
    return this.ngFirestore.collection(this.collection).snapshotChanges();
  }

  getData(id) {
    return this.ngFirestore.collection(this.collection).doc(id).valueChanges();
  }

  update(id, data) {
    return this.ngFirestore.collection(this.collection).doc(id).update(data)
      .then()
      .catch(error => console.error(error));
  }

  delete(id: string) {
    return this.ngFirestore.doc(`${SchemasObject.qrData}/` + id).delete();
  }
}
