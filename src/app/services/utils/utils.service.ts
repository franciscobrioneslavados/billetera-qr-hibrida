import {Injectable} from '@angular/core';
import {FileChooser} from '@ionic-native/file-chooser/ngx';
import {FilePath} from '@ionic-native/file-path/ngx';
import {Platform} from '@ionic/angular';
import {DocumentPicker} from '@ionic-native/document-picker/ngx';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    private fileChooser: FileChooser,
    private filePath: FilePath,
    private platform: Platform,
    private docPicker: DocumentPicker,
  ) {
  }

  async openFile() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('android')) {
        this.fileChooser.open()
          .then(
            uri => {
              this.filePath.resolveNativePath(uri)
                .then(url => {
                  resolve(url);
                })
                .catch(err => console.error(err));
            }
          )
          .catch(error => {
            console.error(error);
          });
      } else {
        this.docPicker.getFile()
          .then(url => {
            resolve(url);
          })
          .catch(e => console.log(e));
      }
    });
  }
}
