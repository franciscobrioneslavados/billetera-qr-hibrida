import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private auth: AngularFireAuth,
  ) {
  }

  public signInAnonymously() {
    this.auth.signInAnonymously().then(data => {
      sessionStorage.setItem('uid', data.user.uid);
    });
  }

}
