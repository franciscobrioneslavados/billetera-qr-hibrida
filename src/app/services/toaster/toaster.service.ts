import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(
    private toastController: ToastController
  ) {
  }

  async successToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }

  async errorToast(header, message, position: 'top' | 'middle' | 'bottom') {
    const toast = await this.toastController.create({
      header,
      message,
      position,
      buttons: [, {
        text: 'Done',
        role: 'cancel',
      }
      ]
    });
    await toast.present();

    const {role} = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }

  async presentToastWithOptions(message, position: 'top' | 'middle' | 'bottom' | undefined, header?) {
    const toast = await this.toastController.create({
      header,
      message,
      position,
      buttons: [
        {
          text: 'Aceptar',
          role: 'ok'

        }, {
          text: 'Cancelar',
          role: 'cancel',

        }
      ]
    });
    await toast.present();
    const {role} = await toast.onDidDismiss();
    return role;
  }

}
