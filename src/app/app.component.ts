import { Component } from '@angular/core';
import {AuthService} from './services/auth/auth.service';

@Component({
  selector: 'app-root',
  template: `<ion-app>
    <ion-router-outlet></ion-router-outlet>
  </ion-app>`,
})
export class AppComponent {
  constructor(
    private auth: AuthService
  ) {
    this.auth.signInAnonymously();
  }
}
