export class DataInterface {
  uid?: string | number;
  name: string;
  url?: string;
  created: Date | string | number;
  detail: string;
  extra?: ExtraDataModel;
}

export class ExtraDataModel {
  docs: DocsExtraModel[];
}

export class DocsExtraModel {
  path: string;
  name: string;
  type: string;
}
