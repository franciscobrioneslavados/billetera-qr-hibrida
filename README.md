# Billetera QR Híbrida

Firmar APK

keytool -genkey -v -keystore release.jks -alias release -keyalg RSA -keysize 2048 -validity 10000 -storepass android -keypass android -dname "cn=Francisco Briones, c=CL"

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore billeteraqr-key.keystore billeteraqr.apk billeteraqr
